#undef NDEBUG
#ifndef __MINUNIT_H__
#define __MINUNIT_H__

#include <stdio.h>
#include <stdlib.h>
#include "dbg.h"

#define mu_suite_start() char *message = NULL;\

#define mu_assert(T, M, ...) if (!(T)) { log_err(M, ##__VA_ARGS__); return M; }
#define mu_run_test(F) debug("\n-----");\
	message = F(); tests_run++; if(message) return message;

#endif
