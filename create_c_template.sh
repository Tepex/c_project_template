#!/bin/sh

usage="\
Usage: $0 [-d DIRECTORY] project_name
       DIRECTORY - project directory relative to \"~/work/\"
"

if [ $# -lt 1 ]; then
	echo "$usage" >&2
	exit 1
fi

dir="$HOME/work/"
project_name=
project_dir=

if [ $1 = "-d" ]; then
	if [ $# -lt 3 ]; then
		echo "$usage" >&2
		exit 1
	else
		project_dir=$2
		project_name=$3
	fi
else
	project_name=$1
fi

if [ -n "$project_dir" ]; then
	dir="$dir$project_dir/$project_name/"
else
	dir="$dir$project_name/"
fi

echo "dir \"$dir\""
echo "project name \"$project_name\""

if [ -e "$dir" ]; then
	echo "Directory $dir already exists." >&2
	exit 1
fi

bin=$dir"bin/"
src=$dir"src/"
inc=$src"inc/"
tests=$dir"tests/"

mkdir -p $bin $inc $tests 
cp LICENSE $dir
cp README.md $dir
cp dbg.h $inc
cp minunit.h $inc
cp runtests.sh $tests
cp lib_tests.c $tests

cp lib_template.c $src"lib"$project_name".c"
cp Makefile $dir
#
# TODO: change 'sed -i --' for Linux 
#
sed -i '' "s/%%project_name%%/$project_name/g" $dir"Makefile"
cp lib_tests.h $inc
sed -i '' "s/%%project_name%%/$project_name/g" $inc"lib_tests.h"

echo "New C project \"$project_name\" created in $dir"
