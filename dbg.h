/*
 * from http://c.learncodethehardway.org/book/ex20.html
 */

#ifndef __DBG_H__
#define __DBG_H__

#include <stdio.h>
#include <errno.h>
#include <string.h>

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log(T, M, ...) fprintf(stderr, "["T"] (function %s: %s:%d: errno: %s) " M "\n", __func__, __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define log_err(M, ...) log("ERROR", M, ##__VA_ARGS__) 
#define log_warn(M, ...) log("WARN", M, ##__VA_ARGS__)
#define log_info(M, ...) log("INFO", M, ##__VA_ARGS__)

#define check(A, M, ...) if(!(A)) {log_err(M, ##__VA_ARGS__); errno = 0; goto error;}
#define sentinel(M, ...) {log_err(M, ##__VA_ARGS__); errno = 0; goto error;}

#define check_mem(A) check(A, "Out of memory.")
#define check_debug(A, M, ...) if(!(A)) {debug(M, ##__VA_ARGS__); errno = 0; goto error;}

#endif
