#include "inc/minunit.h"
#include "inc/lib_tests.h"

#include <dlfcn.h>

void *lib = NULL;
int tests_run = 0;

int check_function(const char *func_to_run, const char *data, int expected)
{
	lib_function func = dlsym(lib, func_to_run);
	check(func != NULL, "Did not find %s function in the library %s: %s", func_to_run, LIB_FILE, dlerror());
	
	int ret = func(data);
	check(ret == expected, "Function %s return %d for data: %s", func_to_run, ret, data);
	return 1;
	
error:
	return 0;
}

char *test_dlopen()
{
	lib = dlopen(LIB_FILE, RTLD_NOW);
	mu_assert(lib != NULL, "Failed to open the library \"%s\" to test.", LIB_FILE);
	return NULL;
}

char *test_dlclose()
{
	int ret = dlclose(lib);
	mu_assert(ret == 0, "Failed to close \"%s\".", LIB_FILE);
	return NULL;
}

char *test_functions()
{
	mu_assert(check_function(FUNC1, "This is data", 0), "%s failed.", FUNC1);
	return NULL;
}

char *test_failures()
{

	return NULL;
}

char *all_tests()
{
	mu_suite_start();
	
	mu_run_test(test_dlopen);
	mu_run_test(test_functions);
	mu_run_test(test_failures);
	mu_run_test(test_dlclose);
	
	return NULL;
}

int main(int argc, char *argv[])
{
	argc = 1;
	debug("----- RUNNING: %s", argv[0]);
	printf("----\nRUNNING: %s\n", argv[0]);
	char *result = all_tests();
	if (result != 0) printf("FAILED: %s\n", result);
	else printf("ALL TESTS PASSED\n");
    printf("Tests run: %d\n", tests_run);
    exit(result != 0);
}
