#include <stdio.h>
#include <ctype.h>

#include "inc/dbg.h"

int my_function(const char *data)
{
	printf("Hello from library!\n%s\n", data);
	return 0;
}
