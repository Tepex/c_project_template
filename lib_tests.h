#ifndef __LIB_TESTS_H__
#define __LIB_TESTS_H__

#define LIB_FILE "./build/lib%%project_name%%.dylib"
#define FUNC1 "my_function"
typedef int (*lib_function)(const char *data);

#endif
